# Copyright (c) 2020 Jerome Arbez-Gindre
# frozen_string_literal: true

require('bundler/setup')

# formatter = [SimpleCov::Formatter::HTMLFormatter]
# SimpleCov.formatter = SimpleCov::Formatter::MultiFormatter.new(formatter)

require('simplecov')

SimpleCov.start do
  command_name 'spec:unit'

  add_group 'Libraries', 'lib'
  add_group 'Unit test', 'spec/unit'

  add_filter 'config'
  add_filter 'vendor'
  # This folowing files is only used to register the extension to
  # asciidoctor : no coverage analysis
  add_filter 'lib/asciidoctor-defmastership.rb'

  minimum_coverage 100

  enable_coverage :branch
end

require('asciidoctor-defmastership')
