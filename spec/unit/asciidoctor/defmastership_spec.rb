# Copyright (c) 2020 Jerome Arbez-Gindre
# frozen_string_literal: true

RSpec.describe(Asciidoctor::Defmastership) do
  it { expect(Asciidoctor::Defmastership::VERSION).not_to(be_nil) }
end
