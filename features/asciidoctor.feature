Feature: Asciidcotor invoaation
  As a Responsible of definitions
  In order to make statistics on definitions
  I want defmastership to extract definitions from asciidoctor documents

  Scenario: We can produce a html file with defmastership definitions
    Given a file named "toto.adoc" with:
    """
    [define, requirement, TOTO-0001]
    --
    Exemple of multiline requirement.
    Second line.
    --
    """
    When I successfully run `asciidoctor -r asciidoctor/defmastership toto.adoc`
    Then the file named "toto.html" should exist

  Scenario: We can invoke asciidoctor-defmastership the correct way
    Given a file named "toto.adoc" with:
    """
    [define, requirement, TOTO-0001]
    --
    Exemple of multiline requirement.
    Second line.
    --
    """
    When I successfully run `asciidoctor -r asciidoctor-defmastership toto.adoc`
    Then the file named "toto.html" should exist
