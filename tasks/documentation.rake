# Copyright (c) 2024 Jerome Arbez-Gindre
# frozen_string_literal: true

namespace 'documentation' do
  require('yard')

  YARD::Rake::YardocTask.new do |task|
    task.files = ['lib/**/*.rb']
    task.options = ['--fail-on-warning', '--verbose']
    task.stats_options = ['--list-undoc']
  end
rescue LoadError
  task(:yard) do
    puts('Install Yard to run its rake tasks')
  end
end

desc 'Synonym for documentation:yard'
task(yard: 'documentation:yard')
