# Copyright (c) 2020 Jerome Arbez-Gindre
# frozen_string_literal: true

lib = File.expand_path('lib', __dir__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require('asciidoctor-defmastership/version')

Gem::Specification.new do |spec|
  spec.metadata = {
    'rubygems_mfa_required' => 'true'
  }

  spec.required_ruby_version = '>= 3.0'
  spec.name          = 'asciidoctor-defmastership'
  spec.version       = Asciidoctor::Defmastership::VERSION
  spec.authors       = ['Jérôme Arbez-Gindre']
  spec.email         = ['jeromearbezgindre@gmail.com']
  spec.licenses      = ['MIT']

  spec.summary = %(asciidoctor extension to handle applicable
                         definition references)

  spec.description = %(asciidoctor-defmastership allows to define
                   applicable definitions references, link to other references
                   (internal or external))

  spec.homepage    = 'https://gitlab.com/defmastership/asciidoctor-defmastership/'

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that
  # have been added into git.
  spec.files =
    Dir.chdir(File.expand_path(__dir__)) do
      `git ls-files -z`.split("\x0").reject do |f|
        f.match(%r{^(test|spec|features)/})
      end
    end
  spec.require_paths = ['lib']

  spec.add_dependency('asciidoctor', '~> 2.0')
  spec.add_dependency('defmastership-core', '~> 1.5')
  spec.add_dependency('ostruct', '~> 0.6')
end
